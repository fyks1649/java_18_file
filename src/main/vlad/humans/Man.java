package vlad.humans;

import vlad.pets.Pet;

import java.util.Map;

public final class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname) {
        super(name, surname);
    }

    public Man(String name, String surname, String birthDate, int iq, Map<String, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public Man(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    void repairCar() {
        System.out.println("чинить авто");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("Привет, " + pet.getSpecies());
    }
}
