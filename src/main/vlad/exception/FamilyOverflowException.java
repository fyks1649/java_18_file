package vlad.exception;

import vlad.humans.Family;

public class FamilyOverflowException extends RuntimeException {

    public FamilyOverflowException(String message) {
        super(message);
    }
}
