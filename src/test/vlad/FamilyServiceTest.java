//package vlad;
//
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import vlad.humans.Family;
//import vlad.humans.Human;
//import vlad.humans.Man;
//import vlad.humans.Woman;
//import vlad.pets.Dog;
//import vlad.pets.Pet;
//
//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//public class FamilyServiceTest {
//    List<Family> families = new ArrayList<>();
//    Human child1 = new Man("Vlad", "lietun");
//    Human child2 = new Man("Nick", "Bubalo");
//    Human child22 = new Man("Igor", "Bubalo");
//    Human child3 = new Man("Denis", "Egorov");
//    Human child33 = new Man("Roma", "Egorov");
//
//    Human woman = new Woman("Elena", "Lietun");
//    Human man = new Man("Dima", "Lietun");
//    Family family = new Family(woman, man);
//
//    Human woman1 = new Woman("Kate", "Bubalo");
//    Human man1 = new Man("Bob", "Bubalo");
//    Family family1 = new Family(woman1, man1);
//
//    Human woman2 = new Woman("Anne", "Egorova");
//    Human man2 = new Man("Oleg", "Egorov");
//    Family family2 = new Family(woman2, man2);
//
//    Set<Pet> pets = new HashSet<>();
//    Pet dog = new Dog("Tuzik", 10, 10, null);
//    Pet dog1 = new Dog("Bobik", 10, 10, null);
//
//    @Before
//    public void setUp() {
//        child1.setYear(1999);
//        child2.setYear(2010);
//        child3.setYear(2001);
//        child33.setYear(2005);
//        family.addChild(child1);
//        family1.addChild(child2);
//        family1.addChild(child22);
//
//        families.add(family);
//        families.add(family1);
//
//        pets.add(dog);
//    }
//
//    public List<Family> getExpectedToString(Family family, Family family1) {
//        List<Family> families = new ArrayList<>();
//        families.add(family);
//        families.add(family1);
//        return families;
//    }
//
//    public String getExpectedToStringList(Family family, Family family1) {
//        return "List of all families: \n 1. " + family + "\n" + "2. " + family1;
//    }
//
//    @Test
//    public void getAllFamiliesPositive() {
//        Assert.assertEquals(getExpectedToString(family, family1).toString(), "[" + family + ", " + family1 + "]");
//    }
//
//    @Test
//    public void getAllFamiliesNegative() {
//        Assert.assertNotEquals(getExpectedToString(family, family1).toString(), "[" + family1 + ", " + family1 + "]");
//    }
//
//    @Test
//    public void displayAllFamiliesPositive() {
//        Assert.assertEquals(getExpectedToStringList(family, family1), "List of all families: \n 1. " + family + "\n" + "2. " + family1);
//    }
//
//    @Test
//    public void displayAllFamiliesNegative() {
//        Assert.assertNotEquals(getExpectedToStringList(family1, family1), "List of all families: \n 1. " + family + "\n" + "2. " + family);
//    }
//
//    @Test
//    public void getFamiliesBiggerThanPositive() {
//        int countFamilyBiggerThan = 3;
//        int result = countFamilyBiggerThan + 1;
//        Assert.assertEquals(result, family1.countFamily());
//    }
//
//    @Test
//    public void getFamiliesBiggerThanNegative() {
//        int countFamilyBiggerThan = 3;
//        int result = countFamilyBiggerThan + 1;
//        Assert.assertNotEquals(result, family.countFamily());
//    }
//
//    @Test
//    public void getFamiliesLessThanPositive() {
//        int countFamilyLessThan = 4;
//        Assert.assertEquals(countFamilyLessThan - 1 , family.countFamily());
//    }
//
//    @Test
//    public void getFamiliesLessThanNegative() {
//        int countFamilyLessThan = 4;
//        Assert.assertNotEquals(countFamilyLessThan - 1 , family1.countFamily());
//    }
//
//    @Test
//    public void countFamiliesWithMemberNumberPositive() {
//        int countPeople = 3;
//        Assert.assertEquals(countPeople, family.countFamily());
//    }
//
//    @Test
//    public void countFamiliesWithMemberNumberNegative() {
//        int countPeople = 3;
//        Assert.assertNotEquals(countPeople, family1.countFamily());
//    }
//
//    @Test
//    public void createNewFamilyPositive() {
//        Assert.assertEquals(getExpectedToString(family, family2).toString(),
//                "[" + family + ", " + new Family(new Woman("Anne", "Egorova"), new Man("Oleg", "Egorov")).toString() + "]");
//    }
//
//    @Test
//    public void createNewFamilyNegative() {
//        Assert.assertNotEquals(getExpectedToString(family, family1).toString(),
//                "[" + family + ", " + new Family(new Woman("Anne", "Egorova"), new Man("Oleg", "Egorov")).toString() + "]");
//    }
//
//    @Test
//    public void deleteFamilyPositive() {
//        List result = getExpectedToString(family, family1);
//        result.remove(family1);
//        Assert.assertEquals("[" +family.toString() + "]",  result.toString());
//    }
//
//    @Test
//    public void deleteFamilyNegative() {
//        List result = getExpectedToString(family, family1);
//        result.remove(family);
//        Assert.assertNotEquals("[" +family.toString() + "]",  result.toString());
//    }
//
//    @Test
//    public void bornChildPositive() {
//        int countPeople = family.countFamily();
//        Human human = new Man("Artur", "Lietun");
//        family.addChild(human);
//        Assert.assertEquals(countPeople + 1, family.countFamily());
//    }
//
//    @Test
//    public void bornChildNegative() {
//        int countPeople = family.countFamily();
//        Human human = new Man("Artur", "Lietun");
//        family1.addChild(human);
//        Assert.assertNotEquals(countPeople + 1, family.countFamily());
//    }
//
//    @Test
//    public void adoptChildPositive() {
//        int countPeople = family.countFamily();
//        Human human = new Man("Artur", "Lietun");
//        family.addChild(human);
//        Assert.assertEquals(countPeople + 1, family.countFamily());
//    }
//
//    @Test
//    public void adoptChildNegative() {
//        int countPeople = family.countFamily();
//        Human human = new Man("Artur", "Lietun");
//        family1.addChild(human);
//        Assert.assertNotEquals(countPeople + 1, family.countFamily());
//    }
//
//    @Test
//    public void deleteAllChildrenOlderThenPositive() {
//        family.addChild(child2);
//        List<Family> resultBefore = getExpectedToString(family, family1);
//        family.getChildren().remove(child2);
//        List<Family> resultAfter = getExpectedToString(family, family1);
//        Assert.assertEquals(resultBefore, resultAfter);
//    }
//
//    @Test
//    public void countPositive() {
//        Assert.assertEquals(2, families.size());
//    }
//
//    @Test
//    public void countNegative() {
//        Assert.assertNotEquals(3, families.size());
//    }
//
//    @Test
//    public void getFamilyByIdPositive() {
//        Assert.assertEquals(family, families.get(0));
//    }
//
//    @Test
//    public void getFamilyByIdNegative() {
//        Assert.assertNotEquals(family, families.get(1));
//    }
//
//    @Test
//    public void getPetsPositive() {
//        family.setPet(pets);
//        String expectedResult = "[" + dog.toString() + "]";
//        Assert.assertEquals(expectedResult, family.getPet().toString());
//    }
//
//    @Test
//    public void getPetsNegative() {
//        pets.add(dog1);
//        family.setPet(pets);
//        String expectedResult = "[" + dog.toString() + "]";
//        Assert.assertNotEquals(expectedResult, family.getPet().toString());
//    }
//
//    @Test
//    public void addPetPositive() {
//        family.setPet(pets);
//        String expectedResult = family.getPet().toString();
//        expectedResult = expectedResult.substring(0, expectedResult.length() - 1) + ", " + dog1 + "]";
//        pets.add(dog1);
//        Assert.assertEquals(expectedResult, family.getPet().toString());
//    }
//
//    @Test
//    public void addPetNegative() {
//        family.setPet(pets);
//        String expectedResult = family.getPet().toString();
//        expectedResult = expectedResult.substring(0, expectedResult.length() - 1) + ", " + dog + "]";
//        pets.add(dog1);
//        Assert.assertNotEquals(expectedResult, family.getPet().toString());
//    }
//}