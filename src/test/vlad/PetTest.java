package vlad;

import org.junit.Assert;
import org.junit.Test;
import vlad.pets.Dog;
import vlad.pets.Pet;

import java.util.HashSet;
import java.util.Set;

public class PetTest {
    private Set<String> strings2 = new HashSet<String>();

    private Dog pet = new Dog("Tuzyk", 5, 50, strings2);
    private Dog pet1 = new Dog("Tuzyk", 5, 50, strings2);

    public String getExpectedToString(Pet pet) {
        String result = pet.getSpecies() +
                "{nickname='" + pet.getNickname() +
                "', age=" + pet.getAge() +
                ", trickLevel=" + pet.getTrickLevel() +
                ", habits=" + pet.getHabits() + "}";
        return result;
    }


    @Test
    public void testToString(){
        Assert.assertEquals(pet.toString(), getExpectedToString(pet));
    }
}